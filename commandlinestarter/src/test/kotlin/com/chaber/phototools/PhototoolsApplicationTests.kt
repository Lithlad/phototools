package com.chaber.phototools

import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@Disabled
@SpringBootTest(args = ["organize", "path"])
class PhototoolsApplicationTests {

    @Test
    fun contextLoads() {
    }
}
