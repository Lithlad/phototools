package com.chaber.phototools

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PhototoolsApplication {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            runApplication<PhototoolsApplication>(*args)
        }
    }
}
