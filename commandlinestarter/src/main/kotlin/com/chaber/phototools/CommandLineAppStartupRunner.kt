package com.chaber.phototools

import com.chaber.phototools.toolsmanager.ToolsManager
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component

@Component
class CommandLineAppStartupRunner(private val toolsManager: ToolsManager) : CommandLineRunner {

    override fun run(vararg args: String?) {
        require(args.isNotEmpty())
        val requestResponse =
            toolsManager.manageRequest(retrieveCalledToolName(args), retrieveCalledToolArguments(args))
        println(requestResponse.getMessage())
    }

    private fun retrieveCalledToolArguments(args: Array<out String?>) = args.asList().drop(1)

    private fun retrieveCalledToolName(args: Array<out String?>) = args[0]
}
