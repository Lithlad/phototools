package com.chaber.phototools.organizer

import com.chaber.phototools.toolsmanager.RequestStatus
import org.assertj.core.api.AbstractBooleanAssert
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.io.TempDir
import java.nio.file.Files
import java.nio.file.Path
import java.util.stream.Collectors
import kotlin.io.path.name
import kotlin.io.path.pathString

internal class OrganizerToolTest {
    companion object {
        var EXPECTED_TOOL_COMMAND = "organize"
    }

    private val organizerTool = OrganizerTool()

    @Test
    fun shouldReturnTrueIfCommandIsAppropriate() {
        // when
        val isAppropriate = organizerTool.isAppropriate(EXPECTED_TOOL_COMMAND)

        // then
        assertThat(isAppropriate).isTrue
    }

    @Test
    fun shouldReturnFalseIfCommandIsAppropriate() {
        // given
        val notSuitableToolCommand = "someCommand"

        // when
        val isAppropriate = organizerTool.isAppropriate(notSuitableToolCommand)

        // then
        assertThat(isAppropriate).isFalse
    }

    @Test
    fun shouldCreateSubDirectoriesOfParentDirectory(@TempDir directoryWithPhotos: Path) {
        val rootDirectoryName = directoryWithPhotos.name

        fun assertThatPhotoFileExists(path: String): AbstractBooleanAssert<*> {
            fun ifPhotoFileExists(): Boolean =
                Files.exists(Path.of("$directoryWithPhotos/$rootDirectoryName$path"))

            return assertThat(ifPhotoFileExists()).isTrue
        }

        fun assertThatPhotoDirectoryIsEmpty(path: String) {
            fun collectPhotoDirectoryObjects(): List<Path> =
                Files.list(Path.of("$directoryWithPhotos/$rootDirectoryName$path"))
                    .collect(Collectors.toUnmodifiableList())

            return assertThat(collectPhotoDirectoryObjects()).isEmpty()
        }

        // given
        val jpgPhoto1Name = "photo1.jpg"
        val jpgPhoto2Name = "photo2.jpg"
        val jpgPhoto3Name = "photo3.JPG"
        val jpgPhoto4Name = "photo4.jpg"
        val jpgPhoto5Name = "photo5.JPG"
        val rawPhoto1Name = "photo1.orf"
        val rawPhoto2Name = "photo2.CR2"
        val rawPhoto3Name = "photo3.ORF"
        val rawPhoto4Name = "photo4.RW2"
        val rawPhoto5Name = "photo5.rw2"
        val movie1Name = "movie1.MOV"
        val movie2Name = "movie1.MP4"
        createFiles(
            directoryWithPhotos,
            setOf(
                jpgPhoto1Name, jpgPhoto2Name, jpgPhoto3Name, jpgPhoto4Name, jpgPhoto5Name,
                rawPhoto1Name, rawPhoto2Name, rawPhoto3Name, rawPhoto4Name, rawPhoto5Name,
                movie1Name, movie2Name
            )
        )

        val args = listOf(directoryWithPhotos.pathString)

        // when
        val requestResult = organizerTool.run(args)

        // then
        assertAll(
            "subdirectories are created",
            { assertThatPhotoFileExists("_raw") },
            { assertThatPhotoFileExists("_jpg") },
            { assertThatPhotoFileExists("_pamiatkowe") },
            { assertThatPhotoFileExists("_produkt") },
            { assertThatPhotoFileExists("_edytowane") },
            { assertThatPhotoFileExists("_z_telefonu") },
            { assertThatPhotoFileExists("_filmy") }
        )
        assertAll(
            "photos are correctly moved",
            { assertThatPhotoFileExists("_raw/43/$rawPhoto1Name") },
            { assertThatPhotoFileExists("_raw/32/$rawPhoto2Name") },
            { assertThatPhotoFileExists("_raw/43/$rawPhoto3Name") },
            { assertThatPhotoFileExists("_raw/43/$rawPhoto4Name") },
            { assertThatPhotoFileExists("_raw/43/$rawPhoto5Name") },
            { assertThatPhotoFileExists("_jpg/$jpgPhoto1Name") },
            { assertThatPhotoFileExists("_jpg/$jpgPhoto2Name") },
            { assertThatPhotoFileExists("_jpg/$jpgPhoto3Name") },
            { assertThatPhotoFileExists("_jpg/$jpgPhoto4Name") },
            { assertThatPhotoFileExists("_jpg/$jpgPhoto5Name") },
            { assertThatPhotoFileExists("_pamiatkowe/$jpgPhoto1Name") },
            { assertThatPhotoFileExists("_pamiatkowe/$jpgPhoto2Name") },
            { assertThatPhotoFileExists("_pamiatkowe/$jpgPhoto3Name") },
            { assertThatPhotoFileExists("_pamiatkowe/$jpgPhoto4Name") },
            { assertThatPhotoFileExists("_pamiatkowe/$jpgPhoto5Name") },
            { assertThatPhotoDirectoryIsEmpty("_produkt") },
            { assertThatPhotoDirectoryIsEmpty("_edytowane") },
            { assertThatPhotoDirectoryIsEmpty("_z_telefonu") },
            { assertThatPhotoFileExists("_filmy/$movie1Name") },
            { assertThatPhotoFileExists("_filmy/$movie2Name") }

        )
        assertThat(requestResult)
            .extracting("resultMessage", "status")
            .containsExactly("Photos are moved correctly", RequestStatus.SUCCESS)
    }

    private fun createFiles(rootDirectory: Path, filesNames: Set<String>) {
        filesNames
            .map { rootDirectory.resolve(it) }
            .forEach { Files.createFile(it) }
    }
}
