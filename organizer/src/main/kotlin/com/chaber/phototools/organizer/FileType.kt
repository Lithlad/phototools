package com.chaber.phototools.organizer

import com.chaber.phototools.organizer.exception.UnknownFileTypeException
import java.io.File

internal enum class FileType {
    FORMAT_32, FORMAT_43, JPG, MOVIE;

    companion object {
        private val FORMAT_JPG_EXTENSIONS = listOf("jpg", "jpeg")
        private val FORMAT_32_EXTENSIONS = listOf("cr2")
        private val FORMAT_43_EXTENSIONS = listOf("orf", "rw2")
        private val MOVIE_EXTENSIONS = listOf("mov", "mp4")

        fun resolveFileType(file: File): FileType {
            val fileExtension = file.resolveExtension()
            if (FORMAT_JPG_EXTENSIONS.contains(fileExtension)) {
                return JPG
            }
            if (FORMAT_32_EXTENSIONS.contains(fileExtension)) {
                return FORMAT_32
            }
            if (FORMAT_43_EXTENSIONS.contains(fileExtension)) {
                return FORMAT_43
            }
            if (MOVIE_EXTENSIONS.contains(fileExtension)) {
                return MOVIE
            }
            throw UnknownFileTypeException(file)
        }
    }
}
