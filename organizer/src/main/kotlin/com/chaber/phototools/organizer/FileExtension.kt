package com.chaber.phototools.organizer

import java.io.File

fun File.resolveExtension(): String {
    val lastIndexOfDot = absolutePath.lastIndexOf('.')
    return absolutePath.substring(lastIndexOfDot + 1).lowercase()
}

val File.fileName get() = toPath().fileName
