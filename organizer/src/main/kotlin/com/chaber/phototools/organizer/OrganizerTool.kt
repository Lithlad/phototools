package com.chaber.phototools.organizer

import com.chaber.phototools.organizer.exception.PassedPathIsNotDirectoryException
import com.chaber.phototools.toolsmanager.Documentation
import com.chaber.phototools.toolsmanager.PhotoTool
import com.chaber.phototools.toolsmanager.RequestResult
import com.chaber.phototools.toolsmanager.SuccessResult
import org.springframework.stereotype.Service
import java.io.File
import java.nio.file.Files
import java.nio.file.Path

private const val TOOL_COMMAND = "organize"
private const val RAW_DIRECTORY_NAME = "raw"
private const val FORMAT_43_DIRECTORY_NAME = "43"
private const val FORMAT_32_DIRECTORY_NAME = "32"
private const val JPG_COMPARISION_DIRECTORY_NAME = "jpg"
private const val JPG_SOUVENIR_DIRECTORY_NAME = "pamiatkowe"
private const val PRODUCT_DIRECTORY_NAME = "produkt"
private const val EDITED_DIRECTORY_NAME = "edytowane"
private const val FROM_MOBILE_DIRECTORY_NAME = "z_telefonu"
private const val MOVIE_DIRECTORY_NAME = "filmy"

/**
 * Class organizes photos in directory structure.
 */
@Service
class OrganizerTool : PhotoTool {

    override fun isAppropriate(name: String): Boolean = TOOL_COMMAND == name

    override fun getDocumentation(): Documentation {
        return Documentation(
            "Organizer",
            "creates structure of photos and puts photos to proper directories",
            TOOL_COMMAND,
            listOf("directory, where photos are placed and where photos should be organized"),
            "organize /path/to/photos/directory"
        )
    }

    /**
     * Method places photos to directories. requires path to directory, whe
     *
     * @param args - list with single element, which is path to directory where photos are placed, and where directory hierarchy should be created.
     */
    override fun run(args: List<String>): RequestResult {
        require(args.size == 1)
        require(args[0].isNotEmpty())
        val rootDirectoryPath = args[0]
        val rootDirectory: File = mapPathToDirectory(rootDirectoryPath)
        val subDirectories: Map<String, File> = createSubDirectories(rootDirectory)
        movePhotosToSubDirectories(rootDirectory, subDirectories)
        return SuccessResult("Photos are moved correctly")
    }

    private fun mapPathToDirectory(rootDirectoryPath: String): File {
        val file = File(rootDirectoryPath)
        if (!file.isDirectory) {
            throw PassedPathIsNotDirectoryException(rootDirectoryPath, getDocumentation())
        }
        return file
    }

    private fun createSubDirectories(rootDirectory: File): Map<String, File> {
        fun createDirectory(parentDirectory: File, subDirectoryName: String): File {
            val directory = File(parentDirectory, "${parentDirectory.name}_$subDirectoryName")
            if (!directory.exists()) {
                directory.mkdir()
            }
            require(directory.exists())
            return directory
        }
        return mapOf(
            RAW_DIRECTORY_NAME to createDirectory(rootDirectory, RAW_DIRECTORY_NAME),
            FORMAT_32_DIRECTORY_NAME to createDirectory(
                rootDirectory,
                "$RAW_DIRECTORY_NAME/$FORMAT_32_DIRECTORY_NAME"
            ),
            FORMAT_43_DIRECTORY_NAME to createDirectory(
                rootDirectory,
                "$RAW_DIRECTORY_NAME/$FORMAT_43_DIRECTORY_NAME"
            ),
            JPG_COMPARISION_DIRECTORY_NAME to createDirectory(rootDirectory, JPG_COMPARISION_DIRECTORY_NAME),
            JPG_SOUVENIR_DIRECTORY_NAME to createDirectory(rootDirectory, JPG_SOUVENIR_DIRECTORY_NAME),
            PRODUCT_DIRECTORY_NAME to createDirectory(rootDirectory, PRODUCT_DIRECTORY_NAME),
            EDITED_DIRECTORY_NAME to createDirectory(rootDirectory, EDITED_DIRECTORY_NAME),
            FROM_MOBILE_DIRECTORY_NAME to createDirectory(rootDirectory, FROM_MOBILE_DIRECTORY_NAME),
            MOVIE_DIRECTORY_NAME to createDirectory(rootDirectory, MOVIE_DIRECTORY_NAME)
        )
    }

    private fun movePhotosToSubDirectories(rootDirectory: File, subDirectories: Map<String, File>) {
        fun joinDirectoryPathWithFileName(directoryType: String, fileName: Path): Path {
            return subDirectories[directoryType]!!.toPath().resolve(fileName)
        }

        fun moveJpgFile(filePathToMove: Path) {
            Files.copy(
                filePathToMove,
                joinDirectoryPathWithFileName(JPG_COMPARISION_DIRECTORY_NAME, filePathToMove.fileName)
            )
            Files.move(
                filePathToMove,
                joinDirectoryPathWithFileName(JPG_SOUVENIR_DIRECTORY_NAME, filePathToMove.fileName)
            )
        }

        fun moveMovieFile(filePathToMove: Path) {
            Files.move(filePathToMove, joinDirectoryPathWithFileName(MOVIE_DIRECTORY_NAME, filePathToMove.fileName))
        }

        val rootDirectoryFiles = rootDirectory.listFiles().toList()
        rootDirectoryFiles
            .filter { it.isFile }
            .filterNotNull()
            .forEach { file ->
                when (FileType.resolveFileType(file)) {
                    FileType.FORMAT_32 -> Files.move(
                        file.toPath(),
                        joinDirectoryPathWithFileName(FORMAT_32_DIRECTORY_NAME, file.fileName)
                    )

                    FileType.FORMAT_43 -> Files.move(
                        file.toPath(),
                        joinDirectoryPathWithFileName(FORMAT_43_DIRECTORY_NAME, file.fileName)
                    )

                    FileType.JPG -> moveJpgFile(file.toPath())
                    FileType.MOVIE -> moveMovieFile(file.toPath())
                }
            }
    }
}
