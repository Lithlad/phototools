package com.chaber.phototools.organizer.exception

import com.chaber.phototools.toolsmanager.Documentation
import com.chaber.phototools.toolsmanager.exception.PhotoToolsException
import java.io.File

class UnknownFileTypeException(file: File)
	: PhotoToolsException("File type of file ${file} is unknown", null)
