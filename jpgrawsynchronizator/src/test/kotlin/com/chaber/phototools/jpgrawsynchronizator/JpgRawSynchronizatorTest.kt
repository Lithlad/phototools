package com.chaber.phototools.jpgrawsynchronizator

import com.chaber.phototools.jpgrawsynchronizator.exceptions.PassedPathIsNotDirectoryException
import com.chaber.phototools.toolsmanager.RequestStatus
import org.assertj.core.api.AbstractBooleanAssert
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.io.TempDir
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.name
import kotlin.io.path.pathString

internal class JpgRawSynchronizatorTest {
    companion object {
        private const val EXPECTED_TOOL_COMMAND = "rawsynchro"
        private const val JPGS_DIRECTORY = "jpg"
        private const val RAW_43_SUBDIRECTORY = "raw/43/"
        private const val RAW_32_SUBDIRECTORY = "raw/32/"
    }

    private val organizerTool = JpgRawSynchronizator()

    @Test
    fun shouldReturnTrueIfCommandIsAppropriate() {
        // when
        val isAppropriate = organizerTool.isAppropriate(EXPECTED_TOOL_COMMAND)

        // then
        assertThat(isAppropriate).isTrue
    }

    @Test
    fun shouldReturnFalseIfCommandIsAppropriate() {
        // given
        val notSuitableToolCommand = "someCommand"

        // when
        val isAppropriate = organizerTool.isAppropriate(notSuitableToolCommand)

        // then
        assertThat(isAppropriate).isFalse
    }

    @Test
    fun shouldThrowExceptionIfRootFileIsNotDirectory(@TempDir projectRoot: Path) {
        // given
        val rawPhoto1 = "photo1.ORF"
        createFiles(setOf(rawPhoto1), projectRoot)
        val args = listOf(projectRoot.resolve(rawPhoto1).pathString)

        // when
        val exception = assertThrows<PassedPathIsNotDirectoryException> { organizerTool.run(args) }

        // then
        assertThat(exception).hasMessageContaining("Passed path is not directory")
    }

    @Test
    fun shouldSynchronizeJpgsWithRawsForDefaultProjectDirectoriesSetup(@TempDir projectRoot: Path) {
        val rootDirectoryName = projectRoot.name
        fun ifPhotoFileExists(subdirectoryPath: String, photoName: String): Boolean =
            Files.exists(projectRoot.resolve("${rootDirectoryName}_$subdirectoryPath$photoName"))

        fun assertThatPhotoFileExists(subdirectoryName: String, photoName: String): AbstractBooleanAssert<*> =
            assertThat(ifPhotoFileExists(subdirectoryName, photoName)).isTrue

        fun assertThatPhotoFileDoesntExist(subdirectoryName: String, photoName: String): AbstractBooleanAssert<*> =
            assertThat(ifPhotoFileExists(subdirectoryName, photoName)).isFalse

        // given
        val rawDirectory = "${rootDirectoryName}_raw"
        val jpgDirectory = "${rootDirectoryName}_$JPGS_DIRECTORY"
        val rawPhoto1 = "photo1.ORF"
        val rawPhoto2 = "photo2.ORF"
        val rawPhoto3 = "photo3.orf"
        val rawPhoto4 = "photo4.cr2"
        val rawPhoto5 = "photo5.cr2"
        val rawPhoto6 = "photo6.CR2"
        val rawPhoto7 = "photo7.rw2"
        val rawPhoto8 = "photo8.RW2"
        val rawPhoto9 = "photo9.RW2"
        val jpgPhotos = setOf("photo1.jpg", "photo3.JPG", "photo5.jpg", "photo6.JPG", "photo7.JPG", "photo9.jpg")
        val orfPhotos = setOf(rawPhoto1, rawPhoto2, rawPhoto3)
        val cr2Photos = setOf(rawPhoto4, rawPhoto5, rawPhoto6)
        val rw2Photos = setOf(rawPhoto7, rawPhoto8, rawPhoto9)
        createDirectories(setOf(rawDirectory, jpgDirectory), projectRoot)
        createDirectories(setOf("43", "32"), projectRoot.resolve(rawDirectory))
        createFiles(jpgPhotos, projectRoot, jpgDirectory)
        createFiles(orfPhotos, projectRoot, rawDirectory, "43")
        createFiles(rw2Photos, projectRoot, rawDirectory, "43")
        createFiles(cr2Photos, projectRoot, rawDirectory, "32")

        val args = listOf(projectRoot.pathString)
        val expectedResultMessage = "Non deleted (error on deletion) 43 photos\n" +
            "\n" +
            "Non deleted (error on deletion) 32 photos\n" +
            "\n" +
            "Deleted 43 photos\n" +
            " photo2.ORF photo8.RW2\n" +
            "Deleted 32 photos\n" +
            " photo4.cr2\n"

        // when
        val requestResult = organizerTool.run(args)

        // then
        assertAll(
            "unwanted orf raws are deleted",
            { assertThatPhotoFileExists(RAW_43_SUBDIRECTORY, rawPhoto1) },
            { assertThatPhotoFileDoesntExist(RAW_43_SUBDIRECTORY, rawPhoto2) },
            { assertThatPhotoFileExists(RAW_43_SUBDIRECTORY, rawPhoto3) },
            { assertThatPhotoFileExists(RAW_43_SUBDIRECTORY, rawPhoto7) },
            { assertThatPhotoFileDoesntExist(RAW_43_SUBDIRECTORY, rawPhoto8) },
            { assertThatPhotoFileExists(RAW_43_SUBDIRECTORY, rawPhoto9) }
        )
        assertAll(
            "unwanted cr2 raws are deleted",
            { assertThatPhotoFileDoesntExist(RAW_32_SUBDIRECTORY, rawPhoto4) },
            { assertThatPhotoFileExists(RAW_32_SUBDIRECTORY, rawPhoto5) },
            { assertThatPhotoFileExists(RAW_32_SUBDIRECTORY, rawPhoto6) }
        )
        assertThat(requestResult)
            .extracting("resultMessage", "status")
            .containsExactly(expectedResultMessage, RequestStatus.SUCCESS)
    }

    private fun createDirectories(directoriesNames: Set<String>, rootDirectory: Path) {
        directoriesNames
            .map { rootDirectory.resolve(it) }
            .forEach { Files.createDirectory(it) }
    }

    private fun createFiles(
        filesNames: Set<String>,
        rootDirectory: Path,
        vararg subdirectory: String
    ) {
        val formattedSubdirectory = subdirectory.joinToString("/")
        val targetDirectory = rootDirectory.resolve(formattedSubdirectory)
        filesNames
            .map { targetDirectory.resolve(it) }
            .forEach { Files.createFile(it) }
    }
}
