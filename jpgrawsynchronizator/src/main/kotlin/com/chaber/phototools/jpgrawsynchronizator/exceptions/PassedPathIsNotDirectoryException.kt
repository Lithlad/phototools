package com.chaber.phototools.jpgrawsynchronizator.exceptions

import com.chaber.phototools.toolsmanager.Documentation
import com.chaber.phototools.toolsmanager.exception.PhotoToolsException

class PassedPathIsNotDirectoryException(directoryPath: String, documentation: Documentation) :
    PhotoToolsException("Passed path is not directory $directoryPath", documentation)
