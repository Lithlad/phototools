package com.chaber.phototools.jpgrawsynchronizator

import com.chaber.phototools.jpgrawsynchronizator.exceptions.PassedPathIsNotDirectoryException
import com.chaber.phototools.toolsmanager.Documentation
import com.chaber.phototools.toolsmanager.PhotoTool
import com.chaber.phototools.toolsmanager.RequestResult
import com.chaber.phototools.toolsmanager.SuccessResult
import org.springframework.stereotype.Service
import java.io.File
import java.nio.file.Files
import java.nio.file.Path

/**
 * Class synchronizes photos between jpg and raw directories (deletes from raw directory not existing jpg files).
 */
@Service
class JpgRawSynchronizator : PhotoTool {
    companion object {
        // TODO use properties file
        private const val TOOL_COMMAND = "rawsynchro"
        private const val DEFAULT_JPG_DIRECTORY = "jpg"
        private const val DEFAULT_RAW_DIRECTORY = "raw"
        private const val DEFAULT_43_DIRECTORY = "43"
        private const val DEFAULT_32_DIRECTORY = "32"
    }

    override fun isAppropriate(name: String): Boolean = TOOL_COMMAND == name

    override fun getDocumentation(): Documentation {
        return Documentation(
            "Jpg-raw synchronizator",
            "basing on jpg directory tool deletes unwaneted raws",
            TOOL_COMMAND,
            listOf("directory, where raws directory and jpgs directory are placed and where photos should be synchronized"),
            "rawsynchro /path/to/photos/directory"
        )
    }

    /**
     * Method places photos to directories. requires path to directory, whe
     *
     * @param args - list with single element, which is path to directory where photos are placed, and where directory hierarchy should be created.
     */
    override fun run(args: List<String>): RequestResult {
        require(args.size == 1)
        require(args[0].isNotEmpty())
        val rootDirectoryPath = args[0]
        val rootDirectory = mapPathToDirectory(rootDirectoryPath)
        val jpgDirectoryPath = "${rootDirectory.name}_$DEFAULT_JPG_DIRECTORY"
        val rawDirectoryPath = "${rootDirectory.name}_$DEFAULT_RAW_DIRECTORY"
        val clearedRaws43Statuses = clearRawsWithoutCorrespondingJpg(
            rootDirectoryPath,
            jpgDirectoryPath,
            rawDirectoryPath,
            DEFAULT_43_DIRECTORY
        )
        val clearedRaws32Statuses = clearRawsWithoutCorrespondingJpg(
            rootDirectoryPath,
            jpgDirectoryPath,
            rawDirectoryPath,
            DEFAULT_32_DIRECTORY
        )
        return SuccessResult(createResultMessage(clearedRaws43Statuses, clearedRaws32Statuses))
    }

    private fun createResultMessage(
        clearedRaws43Statuses: Map<DELETION_STATUS, List<String>>,
        clearedRaws32Statuses: Map<DELETION_STATUS, List<String>>
    ): String {
        fun getPhotosNamesWithStatus(
            deletionStatus: DELETION_STATUS,
            clearedRawsStatuses: Map<DELETION_STATUS, List<String>>
        ): String {
            val photosWithChosenStatus = clearedRawsStatuses[deletionStatus] ?: emptyList()
            return photosWithChosenStatus
                .fold("") { acc, photoName -> "$acc $photoName" }
        }
        return "Non deleted (error on deletion) 43 photos\n" +
            getPhotosNamesWithStatus(DELETION_STATUS.NOT_DELETED, clearedRaws43Statuses) + "\n" +
            "Non deleted (error on deletion) 32 photos\n" +
            getPhotosNamesWithStatus(DELETION_STATUS.NOT_DELETED, clearedRaws32Statuses) + "\n" +
            "Deleted 43 photos\n" +
            getPhotosNamesWithStatus(DELETION_STATUS.DELETED, clearedRaws43Statuses) + "\n" +
            "Deleted 32 photos\n" +
            getPhotosNamesWithStatus(DELETION_STATUS.DELETED, clearedRaws32Statuses) + "\n"
    }

    private fun mapPathToDirectory(rootDirectoryPath: String): File {
        val file = File(rootDirectoryPath)
        if (!file.isDirectory) {
            throw PassedPathIsNotDirectoryException(rootDirectoryPath, getDocumentation())
        }
        return file
    }

    private fun clearRawsWithoutCorrespondingJpg(
        rootDirectory: String,
        jpgDirectory: String,
        vararg rawDirectoryPath: String
    ): Map<DELETION_STATUS, List<String>> {
        val joinedRawDirectory = rawDirectoryPath.joinToString("/", "/")
        val rawPhotosDirectory = mapPathToDirectory("$rootDirectory/$joinedRawDirectory")
        val rawPhotos = rawPhotosDirectory.listFiles().toList()
        val rawPhotosToDelete = rawPhotos
            .asSequence()
            .filter { isCorrespondingJpgDoesntExist(rootDirectory, jpgDirectory, it.nameWithoutExtension) }
            .toList()
            .sorted()
        return deletePhotos(rawPhotosToDelete)
        // todo add log with deleted and non delted photos
    }

    private fun deletePhotos(rawPhotosToDelete: List<File>): Map<DELETION_STATUS, List<String>> {
        return rawPhotosToDelete
            .map { Pair(it.delete(), it.name) }
            .map {
                when (it.first) {
                    true -> Pair(DELETION_STATUS.DELETED, it.second)
                    else -> Pair(DELETION_STATUS.NOT_DELETED, it.second)
                }
            }
            .groupBy({ it.first }, { it.second })
    }

    private fun isCorrespondingJpgDoesntExist(rootDirectory: String, jpgDirectory: String, rawName: String): Boolean {
        return !Files.exists(Path.of(rootDirectory, jpgDirectory, "$rawName.jpg")) &&
            !Files.exists(Path.of(rootDirectory, jpgDirectory, "$rawName.JPG"))
    }

    private enum class DELETION_STATUS {
        DELETED, NOT_DELETED
    }
}
