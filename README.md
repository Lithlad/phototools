# Phototools

## Environment
* `Kotlin`

## Development:
### Command line
* Run main class of commandlinestarter

## Run project
* package main phototools pom
* goto `phototools/commandlinestarter/target`
* java -jar commandlinestarter-x.y.z-SNAPSHOT.jar options