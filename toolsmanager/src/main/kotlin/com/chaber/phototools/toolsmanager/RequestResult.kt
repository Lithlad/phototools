package com.chaber.phototools.toolsmanager

import com.chaber.phototools.toolsmanager.exception.PhotoToolsException

sealed class RequestResult(private val resultMessage: String, private val status: RequestStatus) {

    fun getResultMessage(): String = resultMessage
    fun getStatus(): RequestStatus = status
    abstract fun getMessage(): String
}

enum class RequestStatus {
    SUCCESS,
    DOMAIN_ERROR,
    UNPREDICTED_ERROR
}

class SuccessResult(resultMessage: String) : RequestResult(resultMessage, RequestStatus.SUCCESS) {
    override fun getMessage(): String {
        return "Success!\n${getResultMessage()}"
    }
}

class PhototoolErrorResult(private val photoToolsException: PhotoToolsException) : RequestResult(
    "$OPERATION_FINISHED_UNSUCCESSFULLY_MESSAGE\n${photoToolsException.format()}",
    RequestStatus.DOMAIN_ERROR
) {
    companion object {
        private const val OPERATION_FINISHED_UNSUCCESSFULLY_MESSAGE = "Domain error during processing"
    }

    override fun getMessage(): String {
        return "Phototool error\n${photoToolsException.format()}"
    }
}

class UnpredictedErrorResult(private val exception: Exception) :
    RequestResult("$OPERATION_FINISHED_UNSUCCESSFULLY_MESSAGE\n${exception.message}", RequestStatus.UNPREDICTED_ERROR) {
    companion object {
        private const val OPERATION_FINISHED_UNSUCCESSFULLY_MESSAGE = "Unpredicted error during processing"
    }

    override fun getMessage(): String {
        return "Unpredicted error\n${exception.message}"
    }
}
