package com.chaber.phototools.toolsmanager.exception

import com.chaber.phototools.toolsmanager.Documentation

class ToolArgumentIsNullException(argumentIndex: Int, documentation: Documentation) : PhotoToolsException(
    if (argumentIndex == 0) "Passed photo tool name is null" else "Photo tool parameter on index $argumentIndex is null",
    documentation
)
