package com.chaber.phototools.toolsmanager

import com.chaber.phototools.toolsmanager.exception.NoPhotoToolAvailableException
import com.chaber.phototools.toolsmanager.exception.PhotoToolsException
import com.chaber.phototools.toolsmanager.exception.ToolArgumentIsNullException
import org.springframework.stereotype.Service

@Service
class ToolsManager(
    private val photoTools: Set<PhotoTool>
) {

    companion object {
        private const val DOCUMENTATION_COMMAND = "help"
        private val MANAGER_DOCUMENTATION = Documentation(
            "phototools",
            "Tool to organize photos in directories",
            "java -jar package.jar photo_tool_name photo_tool_args",
            listOf("tool name", "tools arguments"),
            "java -jar commandlinestarter-x.y.z-SNAPSHOT.jar options"
        )
    }

    fun manageRequest(maybeName: String?, args: List<String?>): RequestResult {
        return safeCall {
            val toolName = maybeName ?: throw ToolArgumentIsNullException(0, MANAGER_DOCUMENTATION)

            when (toolName) {
                DOCUMENTATION_COMMAND -> printDocumentation()
                else -> startTool(toolName, args)
            }
        }
    }

    private fun safeCall(operation: () -> RequestResult): RequestResult {
        return try {
            operation()
        } catch (phototoolsException: PhotoToolsException) {
            PhototoolErrorResult(phototoolsException)
        } catch (unpredictedException: Exception) {
            UnpredictedErrorResult(unpredictedException)
        }
    }

    private fun printDocumentation(): SuccessResult {
        val formattedDocumentation = photoTools
            .map { it.getDocumentation() }
            .fold(MANAGER_DOCUMENTATION.format()) { acc, photoToolDocumentation -> "$acc\n${photoToolDocumentation.format()}" }
        return SuccessResult(formattedDocumentation)
    }

    private fun filterNullArguments(args: List<String?>): List<String> {
        return args.map {
            when (it) {
                // TODO pick tool index
                (null) -> throw ToolArgumentIsNullException(-1, MANAGER_DOCUMENTATION)
                else -> it
            }
        }
    }

    private fun startTool(name: String, args: List<String?>): RequestResult {
        val photoTool: PhotoTool = (
            photoTools
                .firstOrNull() { it.isAppropriate(name) }
                ?: throw NoPhotoToolAvailableException(name, MANAGER_DOCUMENTATION)
            )
        return photoTool.run(filterNullArguments(args))
    }
}
