package com.chaber.phototools.toolsmanager

data class Documentation(
    private val name: String,
    private val description: String,
    private val command: String,
    private val arguments: List<String>,
    private val callExample: String
) {

    fun format(): String {
        val formattedArguments = arguments
            .map { "\t-tool argument: $it\n" }
            .reduce { acc, next -> acc + next }

        return "$name: - $description\n" +
            "- tool command: $command\n" +
            formattedArguments +
            "Call: $callExample" +
            "\n\n"
    }
}
