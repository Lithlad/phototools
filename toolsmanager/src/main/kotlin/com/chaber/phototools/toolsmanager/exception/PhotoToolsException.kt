package com.chaber.phototools.toolsmanager.exception

import com.chaber.phototools.toolsmanager.Documentation

open class PhotoToolsException(private val errorMessage: String, private val documentation: Documentation?) :
    Exception(errorMessage) {

    fun format(): String {
        val formattedDocumentation = documentation?.format() ?: ""
        return "${errorMessage}\n$formattedDocumentation"
    }
}
