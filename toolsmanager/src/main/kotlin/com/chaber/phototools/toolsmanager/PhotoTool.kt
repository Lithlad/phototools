package com.chaber.phototools.toolsmanager

interface PhotoTool {

    fun isAppropriate(name: String): Boolean

    fun getDocumentation(): Documentation

    fun run(args: List<String>): RequestResult
}
