package com.chaber.phototools.toolsmanager.exception

import com.chaber.phototools.toolsmanager.Documentation

class NoPhotoToolAvailableException(name: String, documentation: Documentation) :
    PhotoToolsException("No photo tool can be found by using name $name\n", documentation)
