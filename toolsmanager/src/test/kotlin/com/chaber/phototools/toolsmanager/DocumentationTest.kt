package com.chaber.phototools.toolsmanager

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class DocumentationTest {

    @Test
    fun shouldPrintDocumentation() {
        // given
        val name = "sample name"
        val description = "sample description"
        val command = "call command"
        val arguments = listOf("aaa", "bbb")
        val callExample = "call example"
        val documentation = Documentation(name, description, command, arguments, callExample)

        // when
        val formattedDocumentation = documentation.format()

        // then
        assertThat(formattedDocumentation).isEqualTo(
            "sample name: - sample description\n" +
                "- tool command: call command\n" +
                "\t-tool argument: aaa\n" +
                "\t-tool argument: bbb\n" +
                "Call: call example\n\n"
        )
    }
}
