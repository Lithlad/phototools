package com.chaber.phototools.toolsmanager

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.anyList
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito.mock
import org.mockito.Mockito.never
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`

internal class ToolsManagerTest {
    private val photoTool1: PhotoTool = mock(PhotoTool::class.java)

    private val photoTool2: PhotoTool = mock(PhotoTool::class.java)

    private val toolsManager = ToolsManager(setOf(photoTool1, photoTool2))

    @Test
    fun shouldRunAppropriateTool() {
        // given
        val passedName = "tool"
        val resultMessage = "Tool message"
        // TODO replace with kotlin `whenever`
        `when`(photoTool1.isAppropriate(passedName)).thenReturn(false)
        `when`(photoTool2.isAppropriate(passedName)).thenReturn(true)
        `when`(photoTool2.run(anyList())).thenReturn(SuccessResult(resultMessage))
        val passedArguments = emptyList<String>()

        // when
        val requestResponse = toolsManager.manageRequest(passedName, passedArguments)

        // then
        verify(photoTool1, never()).run(anyList())
        verify(photoTool2).run(passedArguments)
        assertThat(requestResponse)
            .extracting("resultMessage", "status")
            .containsExactly(resultMessage, RequestStatus.SUCCESS)
    }

    @Test
    fun shouldThrowExceptionIfNoToolIsFound() {
        // given
        val passedName = "tool"
        `when`(photoTool1.isAppropriate(passedName)).thenReturn(false)
        `when`(photoTool2.isAppropriate(passedName)).thenReturn(false)
        val passedArguments = emptyList<String>()

        // when
        val requestResult = toolsManager.manageRequest(passedName, passedArguments)

        // then
        verify(photoTool1, never()).run(anyList())
        verify(photoTool2, never()).run(anyList())
        assertThat(requestResult.getResultMessage()).contains("No photo tool can be found by using name tool")
        assertThat(requestResult.getStatus()).isEqualTo(RequestStatus.DOMAIN_ERROR)
    }

    @Test
    @Disabled("can not mock documentation class - data class")
    fun shouldPrintDocumentation() {
        // given
        val passedName = "help"
        val documentation1 = mock(Documentation::class.java)
        val documentation2 = mock(Documentation::class.java)
        val formattedDocumentation1 = "formatted documentation 1"
        val formattedDocumentation2 = "formatted documentation 2"
        `when`(documentation1.format()).thenReturn(formattedDocumentation1)
        `when`(documentation2.format()).thenReturn(formattedDocumentation2)
        `when`(photoTool1.getDocumentation()).thenReturn(documentation1)
        `when`(photoTool2.getDocumentation()).thenReturn(documentation2)
        val passedArguments = emptyList<String>()

        // when
        val requestResponse = toolsManager.manageRequest(passedName, passedArguments)

        // then
        verify(photoTool1, never()).isAppropriate(anyString())
        verify(photoTool2, never()).isAppropriate(anyString())
        verify(photoTool1, never()).run(anyList())
        verify(photoTool2, never()).run(anyList())
        verify(photoTool1).getDocumentation()
        verify(photoTool2).getDocumentation()
        verify(documentation1).format()
        verify(documentation2).format()
        assertThat(requestResponse)
            .extracting("resultMessage", "status")
            .containsExactly(
                "Call: java -jar package.jar photo_tool_name photo_tool_args\n" +
                    "$formattedDocumentation1\n" +
                    formattedDocumentation2,
                RequestStatus.SUCCESS
            )
    }
}
